open Alcotest
open Belenios_core.Signatures_core
open Belenios_core.Serializable_core_t
open Belenios_platform.Platform

let create_answer () =
  let module R : RANDOM = struct
    let random _ = Z.one
  end in
  let module G : GROUP = Belenios_core.Ed25519_pure in
  let module Q = Belenios_v1.Question_h.Make (R) (G) in
  let q : Belenios_core.Question_h_t.question =
    {
      q_answers = [| ""; ""; ""; "" |];
      q_blank = None;
      q_min = 1;
      q_max = 1;
      q_question = "";
    }
  in
  let y = G.one in

  let m = [| 0; 1; 0; 0 |] in
  let _, codes = Q.create_answer q ~public_key:y ~prefix:"" m in
  let expected_m = Array.map (fun c -> (c.a + c.b) mod 2) codes in
  check (array int) "v + a = b mod 2" m expected_m

let verify_answer () =
  let module G : GROUP = (val Belenios_v1.Group.of_string "BELENIOS-2048") in
  let module R : RANDOM = struct
    let random x = Z.( - ) x Z.one
  end in
  let module Q = Belenios_v1.Question_h.Make (R) (G) in
  let q : Belenios_core.Question_h_t.question =
    {
      q_answers = [| ""; ""; ""; "" |];
      q_blank = Some true;
      q_min = 1;
      q_max = 1;
      q_question = "";
    }
  in
  let y = G.one in
  let m = [| 1; 0; 0; 0; 0 |] in
  let answers, _ = Q.create_answer q ~public_key:y ~prefix:"aa" m in
  let ok = Q.verify_answer q ~public_key:y ~prefix:"aa" answers in
  check bool "verify_answer" ok true

let suite =
  [
    ("create_answer", `Quick, create_answer);
    ("verify_answer", `Quick, verify_answer);
  ]
