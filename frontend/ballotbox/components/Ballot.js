import React, { createElement as e } from "react";
import { withTranslation } from "react-i18next";
import NiceButton from "../../booth/components/NiceButton";
import { Grid } from "@mui/material";

function TranslatableBallot({
  electionObject,
  tracker,
  mask,
  rawDataUrl,
  filtered,
  t,
}) {
  const [hideMask, setHideMask] = React.useState(true);

  return e(
    Grid,
    {
      container: true,
      className: "ballot" + (filtered ? " ballot-filtered" : ""),
    },
    e(
      Grid,
      {
        item: true,
        xs: 8,
        md: 10,
      },
      e(
        "table",
        { className: "ballot__table" },
        e(
          "tbody",
          null,
          e(
            "tr",
            null,
            e("td", null, t("Tracking number")),
            e("td", null, e("code", null, tracker)),
          ),
          mask.length > 0
            ? e(
                "tr",
                null,
                e("td", null, t("Control pattern")),
                e(
                  "td",
                  null,
                  e(NiceButton, {
                    className: "ballot__mask-button",
                    label: hideMask ? t("Show") : t("Hide"),
                    onClick: () => setHideMask(!hideMask),
                  }),
                  e(
                    "table",
                    { className: hideMask ? "ballot__code-table-hidden" : "" },
                    e(
                      "tbody",
                      null,
                      mask.flatMap((ms, q) => {
                        const maskQ = ms.map((m, a) => {
                          const text = electionObject.questions[q]
                            .blankVoteIsAllowed
                            ? a == 0
                              ? t("blank_vote")
                              : electionObject.questions[q].answers[a - 1]
                            : electionObject.questions[q].answers[a];
                          const textElem = e(
                            "div",
                            { className: "ballot__text" },
                            text,
                          );
                          const masked = e(
                            "div",
                            { className: "ballot__code ballot__code-masked" },
                            " ",
                          );
                          const selected = e(
                            "div",
                            {
                              className:
                                "ballot__code " +
                                (m[0] == 0
                                  ? m[1] == 1
                                    ? "ballot__code-a-1"
                                    : "ballot__code-a-0"
                                  : m[1] == 1
                                  ? "ballot__code-b-1"
                                  : "ballot__code-b-0"),
                            },
                            e(
                              "span",
                              {
                                className:
                                  "ballot__code-symbol material-symbols-outlined",
                              },
                              m[0] == 0
                                ? m[1] == 1
                                  ? "check_box"
                                  : "disabled_by_default"
                                : m[1] == 1
                                ? "thumb_down"
                                : "thumb_up",
                            ),
                          );
                          return e(
                            "tr",
                            { key: `${q}-${a}` },
                            e("td", null, textElem),
                            e("td", null, m[0] == 1 ? masked : selected),
                            e("td", null, m[0] == 0 ? masked : selected),
                          );
                        });
                        if (
                          electionObject.questions[q].blankVoteIsAllowed ===
                          true
                        ) {
                          const s = maskQ.length;
                          return [...maskQ.slice(1, s), maskQ[0]];
                        } else {
                          return maskQ;
                        }
                      }),
                    ),
                  ),
                ),
              )
            : null,
        ),
      ),
    ),
    e(
      Grid,
      {
        item: true,
        xs: 4,
        md: 2,
        className: "ballot__raw_data",
      },
      e(
        "a",
        { className: "nice-button nice-button--white", href: rawDataUrl },
        t("Raw data"),
      ),
    ),
  );
}

const Ballot = withTranslation()(TranslatableBallot);

export { Ballot, TranslatableBallot };
export default Ballot;
