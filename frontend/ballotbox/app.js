import ReactDOM from "react-dom/client";
import React, { createElement as e } from "react";
import i18next from "i18next";
import { withTranslation } from "react-i18next";

import i18n_init from "../booth/i18n_init.js";
import PageHeader from "../booth/components/PageHeader.js";
import { PageFooter, EmptyPageFooter } from "../booth/components/PageFooter.js";
import { NiceTextInput } from "../booth/components/NiceInput.js";
import { Election } from "../booth/election_utils.js";

import Ballot from "./components/Ballot.js";
import NiceButton from "../booth/components/NiceButton.js";

const relativeServerRootFolder = "../../..";

function getHashParametersFromURL() {
  const url_hash_parameters = window.location.hash.substr(1);
  return url_hash_parameters.split("&").reduce(function (result, item) {
    const parts = item.split("=");
    result[parts[0]] = parts[1];
    return result;
  }, {});
}

function BallotBoxPage({ electionObject, electionFingerprint, t, children }) {
  return e(
    "div",
    {
      className: "page",
    },
    e(PageHeader, {
      title: electionObject.title + " - " + t("Accepted ballots"),
      subTitle: electionObject.description,
    }),
    e(
      "div",
      {
        className: "page-body",
        id: "main",
      },
      children,
    ),
    e(PageFooter, {
      electionUuid: electionObject.uuid,
      electionFingerprint: electionFingerprint,
    }),
  );
}
function GenericPage({ title = null, subTitle = null, children }) {
  return e(
    "div",
    {
      className: "page",
    },
    e(PageHeader, {
      title: title,
      subTitle: subTitle,
    }),
    e(
      "div",
      {
        className: "page-body",
      },
      children,
    ),
    e(EmptyPageFooter),
  );
}

function TranslatableBallotBox({
  uuid = null,
  draft,
  tracker = "",
  use_cai = "true",
  t,
}) {
  const searchBoxInputId = "search-input";

  const [electionData, setElectionData] = React.useState({});
  const [electionObject, setElectionObject] = React.useState(undefined);
  const [electionFingerprint, setElectionFingerprint] = React.useState("");
  const [electionLoadingStatus, setElectionLoadingStatus] = React.useState(0);
  const [electionLoadingErrorMessage, setElectionLoadingErrorMessage] =
    React.useState(null);
  const [allBallots, setAllBallots] = React.useState([]);
  const [filteredBallots, setFilteredBallots] = React.useState([]);
  const [otherBallots, setOtherBallots] = React.useState([]);
  const [filter, setFilter] = React.useState(decodeURIComponent(tracker));

  const filterBallots = () => {
    const ballots = filter
      ? allBallots.filter((ballot) => ballot.tracker.includes(filter))
      : allBallots;
    const other = filter
      ? allBallots.filter((ballot) => !ballot.tracker.includes(filter))
      : [];
    const items = ballots.map((ballot) =>
      e(Ballot, {
        electionObject,
        tracker: ballot.tracker,
        rawDataUrl: `${relativeServerRootFolder}/elections/${uuid}/ballot?hash=${encodeURIComponent(
          ballot.hash,
        )}`,
        mask: ballot.mask,
        filtered: false,
      }),
    );
    const otherItems = other.map((ballot) =>
      e(Ballot, {
        electionObject,
        tracker: ballot.tracker,
        rawDataUrl: `${relativeServerRootFolder}/elections/${uuid}/ballot?hash=${encodeURIComponent(
          ballot.hash,
        )}`,
        mask: ballot.mask,
        filtered: true,
      }),
    );
    setFilteredBallots(items);
    setOtherBallots(otherItems);
  };
  const loadBallots = () => {
    const url = `${relativeServerRootFolder}/elections/${uuid}/ballots`;
    fetch(url).then((response) => {
      if (!response.ok) {
        console.log("error loading ballots");
      } else {
        response.json().then((ballotBox) => {
          setAllBallots(ballotBox.ballots);
        });
      }
    });
  };
  React.useEffect(filterBallots, [allBallots, filter]);

  const processElectionData = (inputElectionData) => {
    setElectionData(inputElectionData);
    try {
      let election = new Election(inputElectionData);
      setElectionObject(election);
    } catch (error) {
      setElectionLoadingErrorMessage(error);
      setElectionLoadingStatus(2);
      return;
    }
    setElectionFingerprint(belenios.computeFingerprint(inputElectionData));
    setElectionLoadingStatus(1);
  };
  const loadElectionDataFromUuid = () => {
    const url = draft
      ? `${relativeServerRootFolder}/draft/preview/${uuid}/election.json`
      : `${relativeServerRootFolder}/elections/${uuid}/election.json`;
    fetch(url).then((response) => {
      if (!response.ok) {
        setElectionLoadingErrorMessage(
          "Error: Could not load this election. Maybe no election exists with this identifier.",
        );
        setElectionLoadingStatus(2);
      } else {
        response.json().then(processElectionData);
      }
    });
  };
  React.useMemo(() => {
    if (uuid) {
      loadElectionDataFromUuid();
    }
  }, [uuid]);
  React.useMemo(() => {
    if (uuid && electionObject) {
      loadBallots();
    }
  }, [electionObject]);

  if (!uuid && electionLoadingStatus == 0) {
    return e(
      GenericPage,
      {
        title: "Error",
        subTitle: null,
      },
      e("div", null, "missing election uuid"),
    );
  } else if (electionLoadingStatus === 0 || electionLoadingStatus === 2) {
    const titleMessage = electionLoadingStatus === 0 ? "Loading..." : "Error";
    const loadingMessage =
      electionLoadingStatus === 0 ? titleMessage : electionLoadingErrorMessage;

    return e(
      GenericPage,
      {
        title: titleMessage,
        subTitle: null,
      },
      e(
        "div",
        {
          style: {
            textAlign: "center",
            padding: "30px 0",
          },
        },
        loadingMessage,
      ),
    );
  } else {
    const backButton = e(
      "div",
      { className: "ballotbox__back-button-container" },
      e(
        "a",
        {
          className: "ballotbox__back-button nice-button nice-button--default",
          href: `${relativeServerRootFolder}/elections/${uuid}`,
        },
        t("Back to election"),
      ),
    );
    const searchYourBallot = e(
      "h2",
      { className: "search-control-pattern" },
      t("Check your control pattern"),
    );
    const onFilterUpdate = () => {
      const updatedFilter = document
        .getElementById(searchBoxInputId)
        .value.trim();
      setFilter(updatedFilter);
    };
    const searchBox = e(
      "div",
      { className: "searchbox" },
      e("span", { className: "searchbox__label" }, t("Search tracking number")),
      e(NiceTextInput, {
        id: searchBoxInputId,
        onChange: onFilterUpdate,
        value: filter,
        className: "searchbox__input",
      }),
    );
    const countBallots = e(
      "div",
      { className: "ballot-count" },
      t("ballotbox_show_x_out_of_y", {
        filtered: filteredBallots.length,
        count: allBallots.length,
      }),
    );

    return e(
      BallotBoxPage,
      {
        electionObject,
        electionFingerprint,
        t,
      },
      e(
        "div",
        { className: "page" },
        e(
          "div",
          {
            className: "page-body",
            id: "main",
          },
          tracker !== "" && use_cai === "true" ? searchYourBallot : null,
          searchBox,
          countBallots,
          e(
            "div",
            { className: "ballots-container" },
            ...filteredBallots.concat(otherBallots),
          ),
          backButton,
        ),
      ),
    );
  }
}

const BallotBox = withTranslation()(TranslatableBallotBox);

const afterI18nInitialized = (uuid, draft, tracker, use_cai) => {
  return function () {
    document.title = i18next.t("Ballot box");
    document
      .querySelector("html")
      .setAttribute("lang", i18next.languages[0] || "en");
    const container = document.querySelector("#ballot-box");
    const root = ReactDOM.createRoot(container);
    root.render(
      e(BallotBox, {
        uuid: uuid,
        draft,
        tracker,
        use_cai,
      }),
    );
  };
};

function main() {
  const hash_parameters = getHashParametersFromURL();
  const lang = hash_parameters["lang"];
  const uuid = hash_parameters["uuid"];
  const draft = hash_parameters["draft"];
  const tracker = hash_parameters["tracker"];
  const use_cai = hash_parameters["cai"];
  const container = document.querySelector("#ballot-box");
  container.innerHTML = "Loading...";
  i18n_init(lang, afterI18nInitialized(uuid, draft, tracker, use_cai));
}

main();
