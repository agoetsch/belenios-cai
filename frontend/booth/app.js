import ReactDOM from "react-dom/client";
import React, { createElement as e } from "react";
import i18next from "i18next";
import { withTranslation } from "react-i18next";

import i18n_init from "./i18n_init.js";
import PageHeader from "./components/PageHeader.js";
import { AllQuestionsWithPagination } from "./components/AllQuestionsWithPagination.js";
import NoUuidSection from "./components/NoUuidSection.js";
import InputCredentialSection from "./components/InputCredentialSection.js";
import ReviewEncryptSection from "./components/ReviewEncryptSection.js";
import SelectAuditCodesSection from "./components/SelectAuditCodesSection.js";
import { PageFooter, EmptyPageFooter } from "./components/PageFooter.js";
import { Election } from "./election_utils.js";
import { VoteProgress } from "./components/Progress.js";

const relativeServerRootFolder = "../../..";

function getHashParametersFromURL() {
  const url_hash_parameters = window.location.hash.substr(1);
  return url_hash_parameters.split("&").reduce(function (result, item) {
    const parts = item.split("=");
    result[parts[0]] = parts[1];
    return result;
  }, {});
}

function VotePage({
  electionObject,
  electionFingerprint,
  currentStep,
  useCai,
  children,
}) {
  return e(
    "div",
    {
      className: "page",
    },
    e(PageHeader, {
      title: electionObject.title,
      subTitle: electionObject.description,
    }),
    e(
      "div",
      {
        className: "page-body",
        id: "main", // used to ease targetting of DOM elements in automated tests
      },
      e(VoteProgress, {
        currentStep: currentStep,
        useCai,
      }),
      children,
    ),
    e(PageFooter, {
      electionUuid: electionObject.uuid,
      electionFingerprint: electionFingerprint,
    }),
  );
}

function GenericPage({ title = null, subTitle = null, children }) {
  return e(
    "div",
    {
      className: "page",
    },
    e(PageHeader, {
      title: title,
      subTitle: subTitle,
    }),
    e(
      "div",
      {
        className: "page-body",
      },
      children,
    ),
    e(EmptyPageFooter),
  );
}

function TranslatableVoteApp({
  uuid = null,
  votingCredential = null,
  draft,
  caiPhase = null,
  t,
}) {
  const [currentStep, setCurrentStep] = React.useState(
    votingCredential ? 2 : caiPhase ? 5 : 1,
  ); // Current step of the workflow displayed in the Breadcrumb. 1: input credential. 2: answer questions. 3: review and encrypt. 5: select audit codes.
  const [electionData, setElectionData] = React.useState({});
  const [electionObject, setElectionObject] = React.useState(undefined);
  const [electionFingerprint, setElectionFingerprint] = React.useState("");
  const [credential, setCredential] = React.useState(votingCredential);
  const [useCai, setUseCai] = React.useState(false);
  const [electionLoadingStatus, setElectionLoadingStatus] = React.useState(0); // 0: not yet loaded. 1: loaded with success. 2: loaded with error.
  const [electionLoadingErrorMessage, setElectionLoadingErrorMessage] =
    React.useState(null);
  const [uncryptedBallotBeforeReview, setUncryptedBallotBeforeReview] =
    React.useState(null);
  const [cryptedBallotBeforeReview, setCryptedBallotBeforeReview] =
    React.useState(null);
  const [smartBallotTracker, setSmartBallotTracker] = React.useState(null);

  const processElectionData = (inputElectionData) => {
    setElectionData(inputElectionData);
    try {
      let election = new Election(inputElectionData);
      setElectionObject(election);
    } catch (error) {
      setElectionLoadingErrorMessage(error);
      setElectionLoadingStatus(2);
      return;
    }
    setElectionFingerprint(belenios.computeFingerprint(inputElectionData));
    setElectionLoadingStatus(1);
  };

  const loadElectionDataFromUuid = (uuid, draft) => {
    const url = draft
      ? `${relativeServerRootFolder}/draft/preview/${uuid}/election.json`
      : `${relativeServerRootFolder}/elections/${uuid}/election.json`;
    fetch(url).then((response) => {
      if (!response.ok) {
        setElectionLoadingErrorMessage(
          "Error: Could not load this election. Maybe no election exists with this identifier.",
        ); // TODO: should we localize this?
        setElectionLoadingStatus(2);
      } else {
        response.json().then(processElectionData);
      }
    });
  };

  function loadCai(uuid) {
    setUseCai(true);
    // const url = `${relativeServerRootFolder}/cai/use-cai?uuid=${uuid}`;
    // fetch(url).then((response) => {
    //   if (!response.ok) {
    //     console.log("request failed to " + url);
    //   } else {
    //     response.json().then((r) => {
    //       setUseCai(r.useCai);
    //     });
    //   }
    // });
  }

  React.useMemo(() => {
    if (uuid) {
      loadElectionDataFromUuid(uuid, draft);
      // assuming this is called only once
      loadCai(uuid);
    }
  }, [uuid]);

  const saveCaiData = (ballot, votePrivateData, unencryptedBallot) => {
    sessionStorage.setItem("ballot", ballot);
    sessionStorage.setItem("votePrivateData", votePrivateData);
    sessionStorage.setItem("unencryptedBallot", unencryptedBallot);
  };
  const recoverCaiData = () => [
    sessionStorage.getItem("ballot"),
    sessionStorage.getItem("votePrivateData"),
    sessionStorage.getItem("unencryptedBallot"),
  ];
  const clearCaiData = () => {
    sessionStorage.clear();
  };

  if (!uuid && electionLoadingStatus == 0) {
    const onClickLoadFromParameters = (election_params) => {
      let inputElectionData = null;
      try {
        inputElectionData = JSON.parse(election_params);
      } catch (e) {
        alert(`Election parameters seem to be invalid. Parsing error: ${e}`);
      }
      processElectionData(inputElectionData);
    };

    const onClickLoadFromUuid = (uuid) => {
      // v1:
      // document.location.href = `#uuid=${uuid}`;
      // document.location.reload();

      // v2:
      loadElectionDataFromUuid(uuid, draft);
    };

    const titleMessage = t("page_title");

    return e(
      GenericPage,
      {
        title: titleMessage,
        subTitle: null,
      },
      e(NoUuidSection, {
        onClickLoadFromUuid: onClickLoadFromUuid,
        onClickLoadFromParameters: onClickLoadFromParameters,
      }),
    );
  } else if (electionLoadingStatus === 0 || electionLoadingStatus === 2) {
    const titleMessage = electionLoadingStatus === 0 ? "Loading..." : "Error"; // TODO: should we localize this?
    const loadingMessage =
      electionLoadingStatus === 0 ? titleMessage : electionLoadingErrorMessage;

    return e(
      GenericPage,
      {
        title: titleMessage,
        subTitle: null,
      },
      e(
        "div",
        {
          style: {
            textAlign: "center",
            padding: "30px 0",
          },
        },
        loadingMessage,
      ),
    );
  } else {
    if (currentStep === 1) {
      return e(
        VotePage,
        {
          electionObject,
          electionFingerprint,
          currentStep,
          useCai,
        },
        e(InputCredentialSection, {
          onSubmit: function (credential) {
            if (belenios.checkCredential(credential) === true) {
              setCredential(credential);
              setCurrentStep(2);
            } else {
              alert(t("alert_invalid_credential"));
            }
            return false;
          },
        }),
      );
    } else if (currentStep === 2) {
      return e(
        VotePage,
        {
          electionObject,
          electionFingerprint,
          currentStep,
          useCai,
        },
        e(AllQuestionsWithPagination, {
          electionObject,
          onVoteSubmit: async function (
            event,
            voterSelectedAnswersAsUncryptedBallot,
          ) {
            setUncryptedBallotBeforeReview(
              voterSelectedAnswersAsUncryptedBallot,
            );
            setCryptedBallotBeforeReview(null);
            setSmartBallotTracker(null);
            setCurrentStep(3);
            const encryptBallotSuccessCallback = (
              ballot,
              tracker,
              votePrivateData,
            ) => {
              setCryptedBallotBeforeReview(ballot);
              setSmartBallotTracker(tracker);
              saveCaiData(
                ballot,
                votePrivateData,
                JSON.stringify(voterSelectedAnswersAsUncryptedBallot),
              );
            };
            const encryptBallotErrorCallback = (error) => {
              alert("Error: " + error);
            };
            setTimeout(function () {
              belenios.encryptBallot(
                electionData,
                credential,
                voterSelectedAnswersAsUncryptedBallot,
                encryptBallotSuccessCallback,
                encryptBallotErrorCallback,
              );
            }, 50);
          },
        }),
      );
    } else if (currentStep === 3) {
      const urlToPostEncryptedBallot = `${relativeServerRootFolder}/election/submit-ballot`;
      const onClickPrevious = () => {
        setCurrentStep(2);
        setUncryptedBallotBeforeReview(null);
        setCryptedBallotBeforeReview(null);
        setSmartBallotTracker(null);
      };
      return e(
        VotePage,
        {
          electionObject,
          electionFingerprint,
          currentStep,
          useCai,
        },
        e(ReviewEncryptSection, {
          electionObject,
          uncryptedBallot: uncryptedBallotBeforeReview,
          cryptedBallot: cryptedBallotBeforeReview,
          smartBallotTracker,
          onClickPrevious,
          urlToPostEncryptedBallot,
          draft,
        }),
      );
    } else if (currentStep === 5) {
      const [ballot, votePrivateData, unencryptedBallot] = recoverCaiData();
      if (
        ballot === null ||
        votePrivateData === null ||
        unencryptedBallot === null
      ) {
        return e(
          GenericPage,
          {
            title: "Error",
            subTitle: null,
          },
          e(
            "div",
            {
              style: {
                textAlign: "center",
                padding: "30px 0",
              },
            },
            "Missing data in local storage",
          ),
        );
      } else {
        const urlToPostEncryptedBallot = `${relativeServerRootFolder}/election/submit-ballot-cai`;
        const onClickPrevious = () => {
          setCurrentStep(1);
          setCryptedBallotBeforeReview(null);
          setSmartBallotTracker(null);
          clearCaiData();
          window.location.hash = `uuid=${uuid}`;
        };
        const onPrePost = () => {
          clearCaiData();
        };
        return e(
          VotePage,
          {
            electionObject,
            electionFingerprint,
            currentStep,
            useCai,
          },
          e(SelectAuditCodesSection, {
            electionData,
            electionObject,
            uncryptedBallot: JSON.parse(unencryptedBallot),
            cryptedBallot: ballot,
            votePrivateData: JSON.parse(votePrivateData),
            onClickPrevious,
            onPrePost,
            urlToPostEncryptedBallot,
            t,
          }),
        );
      }
    }
  }
}

const VoteApp = withTranslation()(TranslatableVoteApp);

const afterI18nInitialized = (uuid, credential, draft, caiPhase) => {
  return function () {
    document.title = i18next.t("page_title");
    document
      .querySelector("html")
      .setAttribute("lang", i18next.languages[0] || "en");
    const container = document.querySelector("#vote-app");
    const root = ReactDOM.createRoot(container);
    root.render(
      e(VoteApp, {
        votingCredential: credential,
        uuid: uuid,
        draft,
        caiPhase,
      }),
    );
  };
};

function main() {
  const hash_parameters = getHashParametersFromURL();
  const lang = hash_parameters["lang"];
  const uuid = hash_parameters["uuid"];
  const credential = hash_parameters["credential"];
  const draft = hash_parameters["draft"];
  const caiPhase = hash_parameters["cai"];
  const container = document.querySelector("#vote-app");
  container.innerHTML = "Loading...";
  i18n_init(lang, afterI18nInitialized(uuid, credential, draft, caiPhase));
}

main();
