import React, { createElement as e } from "react";
import { withTranslation } from "react-i18next";

import { Progress } from "./Progress.js";
import { BlueNiceButton, NiceButton, WhiteNiceButton } from "./NiceButton.js";
import SelectAuditCodesTable from "./SelectAuditCodesTable.js";

function TranslatableSelectAuditCodesSection({
  electionData = null,
  electionObject = null,
  uncryptedBallot = [],
  cryptedBallot = null,
  votePrivateData = null,
  onClickPrevious = null,
  urlToPostEncryptedBallot = "",
  onPrePost,
  t,
}) {
  const ballotContainerId = "ballot_div";
  const ballotFormId = "ballot_form";
  const encryptedBallotCaiId = "ballot";
  const encryptedBallotCaiName = "ballot_cai";

  const checkDone = uncryptedBallot.map((q) =>
    q.map((_) => React.useState(false)),
  );
  const [selectable, setSelectable] = React.useState(false);
  const selectionDone = uncryptedBallot.map((q) =>
    q.map((_) => React.useState(false)),
  );
  const selectedCode = uncryptedBallot.map((q) =>
    q.map((_) => React.useState(-1)),
  );
  const [copyable, setCopyable] = React.useState(false);
  const [displayErrorNotChecked, setDisplayErrorNotChecked] =
    React.useState(false);
  const [displayErrorNotSelected, setDisplayErrorNotSelected] =
    React.useState(false);

  const submit = () => {
    const auditSelection = selectedCode.map((q) => q.map((o) => o[0]));
    belenios.revealMask(
      electionData,
      JSON.parse(cryptedBallot),
      votePrivateData,
      auditSelection,
      (ballotCaiUpdate) => {
        onPrePost();
        document.getElementById(encryptedBallotCaiId).value = ballotCaiUpdate;
        document.getElementById(ballotFormId).submit();
      },
      (error) => {
        alert("Error: " + error);
      },
    );
  };

  const clickPrevious = () => {
    if (copyable) {
      setCopyable(false);
      selectionDone.forEach((q) => q.forEach((a) => a[1](false)));
      selectedCode.forEach((q) => q.forEach((a) => a[1](-1)));
    } else if (selectable) {
      setSelectable(false);
      checkDone.forEach((q) => q.forEach((a) => a[1](false)));
      selectionDone.forEach((q) => q.forEach((a) => a[1](false)));
      selectedCode.forEach((q) => q.forEach((a) => a[1](-1)));
      setDisplayErrorNotSelected(false);
    } else if (checkDone.flat(1).findIndex((s) => s[0]) != -1) {
      checkDone.forEach((q) => q.forEach((a) => a[1](false)));
      setDisplayErrorNotChecked(false);
    } else {
      if (confirm(t("progress_will_be_lost"))) {
        onClickPrevious();
      }
    }
  };

  const clickNext = () => {
    if (copyable) {
      submit();
    } else if (selectable) {
      if (selectionDone.flat(2).findIndex((s) => !s) != -1) {
        setDisplayErrorNotSelected(true);
      } else {
        setDisplayErrorNotSelected(false);
        setCopyable(true);
      }
    } else {
      if (checkDone.flat(2).findIndex((s) => !s) != -1) {
        setDisplayErrorNotChecked(true);
      } else {
        setDisplayErrorNotChecked(false);
        setSelectable(true);
      }
    }
  };

  const encryptedBallotField = e(
    "div",
    {
      style: {
        display: "none",
      },
    },
    t("encrypted_ballot_is"),
    e("textarea", {
      readOnly: "readonly",
      cols: "80",
      rows: "1",
      value: cryptedBallot ? cryptedBallot : undefined,
    }),
  );
  const encryptedBallotCaiField = e(
    "div",
    {
      style: {
        display: "none",
      },
    },
    "Encrypted ballot with cast-as-intended:",
    e("textarea", {
      id: encryptedBallotCaiId,
      name: encryptedBallotCaiName,
      readOnly: "readonly",
      cols: "80",
      rows: "1",
      value: "",
    }),
  );

  const errorNotChecked = e(
    "p",
    {
      className: "select-audit-codes__error-not-filled",
      style: {
        display: displayErrorNotChecked ? "block" : "none",
      },
    },
    t("compare_each_line"),
  );
  const errorNotSelected = e(
    "p",
    {
      className: "select-audit-codes__error-not-filled",
      style: {
        display: displayErrorNotSelected ? "block" : "none",
      },
    },
    t("One symbol needs to be selected for each line"),
  );

  const navigationButtonStyle = {
    padding: "10px 13px",
    minWidth: "38px",
  };
  const previousButton = e(NiceButton, {
    tagName: "a",
    label: t("previous_button_label"),
    style: {
      ...navigationButtonStyle,
      marginRight: "20px",
    },
    onClick: clickPrevious,
  });
  const pagination = e(
    "div",
    {
      style: {
        marginTop: "20px",
        textAlign: "center",
      },
    },
    previousButton,
    e(BlueNiceButton, {
      tagName: "a",
      label: t("next_button_label"),
      style: {
        ...navigationButtonStyle,
        marginLeft: "20px",
      },
      onClick: clickNext,
    }),
  );

  return e(
    "div",
    {
      id: ballotContainerId,
    },
    e(
      "form",
      {
        id: ballotFormId,
        method: "POST",
        action: urlToPostEncryptedBallot,
        encType: "multipart/form-data",
      },
      encryptedBallotField,
      encryptedBallotCaiField,
      e(
        "div",
        {
          className: "select-audit-codes",
        },
        e("h2", null, t("Security check ")),
        e(
          "ul",
          null,
          e(
            "li",
            {
              className:
                copyable || selectable
                  ? "select-audit-codes__instructions-done"
                  : "select-audit-codes__instructions-doing",
            },
            t("audit_codes_step_check"),
          ),
          e(
            "li",
            {
              className: copyable
                ? "select-audit-codes__instructions-done"
                : selectable
                ? "select-audit-codes__instructions-doing"
                : "select-audit-codes__instructions-todo",
            },
            t("audit_codes_step_select"),
          ),
          e(
            "li",
            {
              className: copyable
                ? "select-audit-codes__instructions-doing"
                : "select-audit-codes__instructions-todo",
            },
            t("audit_codes_step_save"),
          ),
        ),
        e(
          "div",
          {
            className: "select-audit-codes__info",
          },
          e(
            "span",
            { className: "tooltip" },
            e("p", null, t("cai_more_infos_a")),
            e("p", null, t("cai_more_infos_b")),
            e("p", null, t("cai_more_infos_c")),
          ),
          e(
            "span",
            {
              className:
                "material-symbols-outlined select-audit-codes__info-icon",
            },
            "info",
          ),
          e(
            "span",
            { className: "select-audit-codes__info-text" },
            t("More info"),
          ),
        ),
        e(SelectAuditCodesTable, {
          electionObject,
          uncryptedBallot,
          votePrivateData,
          checkDone,
          selectable,
          selectionDone,
          selectedCode,
          copyable,
          setDisplayErrorNotChecked,
          setDisplayErrorNotSelected,
          t,
        }),
        errorNotChecked,
        errorNotSelected,
        pagination,
      ),
    ),
  );
}

const SelectAuditCodesSection = withTranslation()(
  TranslatableSelectAuditCodesSection,
);

export { SelectAuditCodesSection, TranslatableSelectAuditCodesSection };
export default SelectAuditCodesSection;
