import React, { createElement as e } from "react";
import { withTranslation } from "react-i18next";
import { Grid } from "@mui/material";

import { markup } from "../shortcuts.js";
import SelectAuditCodesCheck from "./SelectAuditCodesCheck.js";

function TranslatableSelectAuditCodesQuestion({
  question,
  questionIndex,
  uncryptedBallot,
  votePrivateData,
  checkDone,
  selectable,
  selectionDone,
  selectedCode,
  copyable,
  setDisplayErrorNotChecked,
  setDisplayErrorNotSelected,
  t,
}) {
  const questionText = question.title;
  const questionPossibleAnswers = question.answers;
  const numberAnswers = uncryptedBallot[questionIndex].length;
  const renderedAnswers = uncryptedBallot[questionIndex].map(
    function (answer, answer_index) {
      const index =
        question.blankVoteIsAllowed === true ? answer_index - 1 : answer_index;
      const isChecked = checkDone[answer_index][0];
      const isSelected = selectionDone[answer_index][0];
      const previousIsChecked = question.blankVoteIsAllowed
        ? answer_index === 0
          ? checkDone[numberAnswers - 1][0]
          : answer_index === 1
          ? true // TODO fix for multiple quetions
          : checkDone[answer_index - 1][0]
        : answer_index === 0
        ? true
        : checkDone[answer_index - 1][0];
      const previousIsSelected = question.blankVoteIsAllowed
        ? answer_index === 0
          ? selectionDone[numberAnswers - 1][0]
          : answer_index === 1
          ? true
          : selectionDone[answer_index - 1][0]
        : answer_index === 0
        ? true
        : selectionDone[answer_index - 1][0];

      const isActive =
        (previousIsChecked && !isChecked) ||
        (selectable && previousIsSelected && !isSelected);

      return e(
        Grid,
        {
          xs: 12,
          item: true,
        },
        e(
          "div",
          {
            className:
              (previousIsChecked ? "" : "cai-table-line-disabled") +
              " " +
              (isActive ? "cai-table-line-active" : "cai-table-line-inactive"),
          },
          e(
            Grid,
            {
              container: true,
              columnSpacing: 2,
            },
            e(
              Grid,
              {
                item: true,
                xs: 4,
              },
              e(
                Grid,
                {
                  container: true,
                  className:
                    "cai-table-vote-option" +
                    (!selectable && answer === 1
                      ? " cai-table-vote-option-selected"
                      : "") +
                    (selectable && !copyable
                      ? " cai-table-vote-option-hidden-hidden"
                      : "") +
                    (copyable ? " cai-table-vote-option-hidden" : ""),
                },
                e(
                  Grid,
                  {
                    item: true,
                    xs: 8,
                    md: 10,
                    className: "cai-table-vote-option-text",
                  },
                  answer_index === 0 && question.blankVoteIsAllowed === true
                    ? t("blank_vote")
                    : markup(questionPossibleAnswers[index]),
                ),
                e(
                  Grid,
                  {
                    item: true,
                    xs: 4,
                    md: 2,
                    className:
                      "cai-table-answer" +
                      (answer === 1 ? " cai-table-answer-selected" : ""),
                  },
                  answer === 1
                    ? e(
                        "span",
                        { className: "material-symbols-outlined" },
                        "check_box",
                      )
                    : e(
                        "span",
                        { className: "material-symbols-outlined" },
                        "disabled_by_default",
                      ),
                ),
              ),
            ),
            e(
              Grid,
              {
                item: true,
                xs: 8,
              },
              e(SelectAuditCodesCheck, {
                valueA: votePrivateData[answer_index].a,
                valueB: votePrivateData[answer_index].b,
                checkable: previousIsChecked,
                checkDone: checkDone[answer_index][0],
                setCheckDone: checkDone[answer_index][1],
                selectable: selectable && previousIsSelected,
                selectionDone: selectionDone[answer_index][0],
                setSelectionDone: selectionDone[answer_index][1],
                selectedCode: selectedCode[answer_index],
                copyable,
                setDisplayErrorNotChecked,
                setDisplayErrorNotSelected,
                t,
              }),
            ),
          ),
        ),
      );
    },
  );
  const renderedAnswersBlankAtEnd =
    question.blankVoteIsAllowed === true
      ? [...renderedAnswers.slice(1), renderedAnswers[0]]
      : renderedAnswers;

  const renderedVoteToQuestion = e(
    React.Fragment,
    null,
    e(
      "h3",
      {
        className: "whole-vote-recap__question-title",
      },
      markup(questionText),
    ),
    e(
      Grid,
      {
        className: "cai-table",
        container: true,
        rowSpacing: 1,
        columnSpacing: 1,
      },
      e(
        Grid,
        {
          xs: 4,
          item: true,
        },
        e("div", { className: "cai-table-header" }, t("your_vote")),
      ),
      e(
        Grid,
        {
          xs: 8,
          item: true,
        },
        e(
          Grid,
          {
            container: true,
            columnSpacing: 1,
          },
          e(
            Grid,
            {
              className: "cai-table-header cai-table-col-audit-code",
              xs: copyable || selectable ? 4 : 2,
              md: copyable || selectable ? 2 : 1,
              item: true,
            },
            copyable || selectable ? t("Control pattern") : t("control_value"),
          ),
        ),
      ),
      ...renderedAnswersBlankAtEnd,
    ),
  );
  return e("div", null, renderedVoteToQuestion);
}

const SelectAuditCodesQuestion = withTranslation()(
  TranslatableSelectAuditCodesQuestion,
);

export { SelectAuditCodesQuestion, TranslatableSelectAuditCodesQuestion };
export default SelectAuditCodesQuestion;
