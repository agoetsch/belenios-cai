import React, { createElement as e } from "react";
import { withTranslation } from "react-i18next";

import { QuestionTypeEnum } from "../election_utils.js";
import SelectAuditCodesQuestion from "./SelectAuditCodesQuestion.js";
import { NiceButton } from "./NiceButton.js";

async function convertImgToDataURL(url) {
  return new Promise((resolve, reject) => {
    var img = new Image();
    img.crossOrigin = "Anonymous";
    img.onload = function () {
      var canvas = document.createElement("CANVAS");
      var ctx = canvas.getContext("2d");
      var dataURL;
      canvas.height = this.height;
      canvas.width = this.width;
      ctx.drawImage(this, 0, 0);
      dataURL = canvas.toDataURL("image/png");
      resolve(dataURL);
    };
    img.onerror = (error) => {
      reject(error);
    };
    img.src = url;
  });
}

function TranslatableSelectAuditCodesTable({
  electionObject = null,
  uncryptedBallot = [],
  votePrivateData = null,
  checkDone,
  selectable,
  selectionDone,
  selectedCode,
  copyable,
  setDisplayErrorNotChecked,
  setDisplayErrorNotSelected,
  t,
}) {
  const [copied, setCopied] = React.useState(false);

  const renderedQuestions = electionObject.questions.map(
    function (question, questionIndex) {
      const questionType = question.type;
      let questionRecapComponent;
      if (questionType === QuestionTypeEnum.CLASSIC) {
        questionRecapComponent = SelectAuditCodesQuestion;
      } else {
        return e("div", null, "Error: Unknown question type.");
      }
      return e(questionRecapComponent, {
        question,
        questionIndex,
        uncryptedBallot,
        votePrivateData: votePrivateData
          ? votePrivateData[questionIndex]
          : null,
        checkDone: checkDone[questionIndex],
        selectable: selectable,
        selectionDone: selectionDone[questionIndex],
        selectedCode: selectedCode[questionIndex],
        copyable,
        setDisplayErrorNotChecked,
        setDisplayErrorNotSelected,
        t,
      });
    },
  );

  const copyAuditCodes = async (e) => {
    e.preventDefault();
    const width = 35;
    const checked = await convertImgToDataURL("../../pdfimgs/checked.png");
    const unchecked = await convertImgToDataURL("../../pdfimgs/unchecked.png");
    const yes = await convertImgToDataURL("../../pdfimgs/yes.png");
    const no = await convertImgToDataURL("../../pdfimgs/no.png");
    const blank = await convertImgToDataURL("../../pdfimgs/blank.png");

    const pattern = selectedCode
      .map((questionState, questionIndex) => {
        const lines = questionState.map((answerState, answerIndex) => {
          const answerText = electionObject.questions[questionIndex]
            .blankVoteIsAllowed
            ? answerIndex == 0
              ? t("blank_vote")
              : electionObject.questions[questionIndex].answers[answerIndex - 1]
            : electionObject.questions[questionIndex].answers[answerIndex];
          const answerTextElem = { text: answerText };
          if (answerState[0] === 0) {
            if (votePrivateData[questionIndex][answerIndex].a === 1) {
              return [
                answerTextElem,
                { image: checked, width: width },
                { image: blank, width: width },
              ];
            } else {
              return [
                answerTextElem,
                { image: unchecked, width: width },
                { image: blank, width: width },
              ];
            }
          } else {
            if (votePrivateData[questionIndex][answerIndex].b === 1) {
              return [
                answerTextElem,
                { image: blank, width: width },
                { image: no, width: width },
              ];
            } else {
              return [
                answerTextElem,
                { image: blank, width: width },
                { image: yes, width: width },
              ];
            }
          }
        });
        const linesInOrder = electionObject.questions[questionIndex]
          .blankVoteIsAllowed
          ? [...lines.slice(1), lines[0]]
          : lines;
        return linesInOrder;
      })
      .flat();
    var docDefinition = {
      content: [
        t("your_control_pattern"),
        "\n\n",
        {
          layout: "noBorders",
          table: {
            body: pattern,
          },
        },
      ],
    };
    pdfMake.fonts = {
      Roboto: {
        normal: "http://localhost:8001/static/fonts/Roboto-Regular.ttf",
        // normal: "https://belenios.loria.fr/cai/static/fonts/Roboto-Regular.ttf", // I know I know...
      },
    };
    pdfMake.createPdf(docDefinition).download("control-pattern.pdf");
  };

  const copyCodesButton = e(NiceButton, {
    className: "cai-table-save-button",
    onClick: copyAuditCodes,
    label: t("Copy control pattern"),
  });

  return e(
    "div",
    null,
    e(
      "div",
      {
        className: "cai-table-group",
      },
      ...renderedQuestions,
    ),
    e(
      "div",
      {
        className:
          "cai-table-save" + (copyable ? "" : " cai-table-save-hidden"),
      },
      e(
        "div",
        null,
        e(
          "div",
          { className: "material-symbols-outlined cai-table-save-icon" },
          "download",
        ),
      ),
      copyCodesButton,
    ),
  );
}

const SelectAuditCodesTable = withTranslation()(
  TranslatableSelectAuditCodesTable,
);

export { SelectAuditCodesTable, TranslatableSelectAuditCodesTable };
export default SelectAuditCodesTable;
