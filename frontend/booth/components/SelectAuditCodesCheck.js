import React, { createElement as e } from "react";
import { Grid } from "@mui/material";
import { withTranslation } from "react-i18next";

function TranslatableSelectAuditCodesCheck({
  valueA,
  valueB,
  checkable,
  checkDone,
  setCheckDone,
  selectable,
  selectionDone,
  setSelectionDone,
  selectedCode,
  copyable,
  setDisplayErrorNotChecked,
  setDisplayErrorNotSelected,
  t,
}) {
  const [checkMistake, setCheckMistake] = React.useState(false);
  const [checkMistakeValue, setCheckMistakeValue] = React.useState(-1);

  function clickCheckButton(selectedValue) {
    return () => {
      setDisplayErrorNotChecked(false);
      if (selectedValue != valueB) {
        setCheckMistake(true);
        setCheckMistakeValue(selectedValue);
      } else {
        setCheckMistake(false);
        setCheckDone(true);
      }
    };
  }

  function selectCode(selection) {
    return () => {
      if (selectable && !selectionDone) {
        selectedCode[1](selection);
        setSelectionDone(true);
        setDisplayErrorNotSelected(false);
      }
    };
  }

  const codeA = e(
    Grid,
    { item: true, xs: 2, md: 1, className: "cai-table-col-audit-code" },
    e(
      "div",
      {
        onClick: selectCode(0),
        className:
          "cai-table-audit-code " +
          (valueA === 1
            ? "cai-table-audit-code-a-1"
            : "cai-table-audit-code-a-0") +
          (selectable && !selectionDone && valueA === 1
            ? " cai-table-audit-code-a-1-selectable"
            : "") +
          (selectable && !selectionDone && valueA === 0
            ? " cai-table-audit-code-a-0-selectable"
            : "") +
          (selectionDone && selectedCode[0] === 0
            ? " cai-table-audit-code-selected"
            : "") +
          (selectionDone && selectedCode[0] === 1 && copyable
            ? " cai-table-audit-code-not-selected-copyable"
            : ""),
      },
      e(
        "span",
        { className: "material-symbols-outlined" },
        valueA === 1 ? "check_box" : "disabled_by_default",
      ),
    ),
  );
  const codeB = e(
    Grid,
    { item: true, xs: 2, md: 1, className: "cai-table-col-audit-code" },
    e(
      "div",
      {
        onClick: selectCode(1),
        className:
          "cai-table-audit-code " +
          (valueB === 1
            ? "cai-table-audit-code-b-1"
            : "cai-table-audit-code-b-0") +
          (selectable && !selectionDone && valueB === 1
            ? " cai-table-audit-code-b-1-selectable"
            : "") +
          (selectable && !selectionDone && valueB === 0
            ? " cai-table-audit-code-b-0-selectable"
            : "") +
          (selectionDone && selectedCode[0] === 1
            ? " cai-table-audit-code-selected"
            : "") +
          (selectionDone && selectedCode[0] === 0 && copyable
            ? " cai-table-audit-code-not-selected-copyable"
            : ""),
      },
      e(
        "span",
        { className: "material-symbols-outlined" },
        valueB === 1 ? "thumb_down" : "thumb_up",
      ),
    ),
  );
  const check = e(
    Grid,
    {
      item: true,
      xs: 12,
      md: 9,
      className: checkable ? "" : "cai-table-check-group-disabled",
    },
    e(
      "div",
      {
        className: "cai-table-check-group",
      },
      e(
        "div",
        { className: "cai-table-check-text" },
        t("are_symbols_identical"),
      ),
      e(
        "div",
        {
          className:
            "cai-table-check-button cai-table-check-yes" +
            (checkMistake && checkMistakeValue === 0
              ? " cai-table-check-button-mistake"
              : ""),
          onClick: clickCheckButton(0),
        },
        t("yes"),
        e(
          "span",
          {
            className:
              "material-symbols-outlined cai-table-check-button-icon cai-table-check-button-icon-yes",
          },
          "thumb_up",
        ),
      ),
      e(
        "div",
        {
          className:
            "cai-table-check-button cai-table-check-no" +
            (checkMistake && checkMistakeValue === 1
              ? " cai-table-check-button-mistake"
              : ""),
          onClick: clickCheckButton(1),
        },
        t("no"),
        e(
          "span",
          {
            className: "material-symbols-outlined cai-table-check-button-icon",
          },
          "thumb_down",
        ),
      ),
    ),
  );

  const pick = e(
    Grid,
    { item: true, xs: 12, md: 10 },
    e("div", { className: "cai-table-check-message" }, t("pick_one_symbol")),
  );

  return e(
    Grid,
    { container: true, columnSpacing: 2, rowSpacing: 1 },
    codeA,
    checkDone ? codeB : check,
    checkMistake
      ? e(
          Grid,
          { item: true, xs: 12, md: 2 },
          e("div", { className: "cai-table-check-mistake" }, t("check_again")),
        )
      : null,
    selectable && !selectionDone ? pick : null,
  );
}

const SelectAuditCodesCheck = withTranslation()(
  TranslatableSelectAuditCodesCheck,
);

export { SelectAuditCodesCheck, TranslatableSelectAuditCodesCheck };
export default SelectAuditCodesCheck;
