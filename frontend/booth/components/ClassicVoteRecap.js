import React, { createElement as e } from "react";
import { withTranslation } from "react-i18next";
import { Grid } from "@mui/material";
import { markup } from "../shortcuts.js";

function TranslatableClassicVoteRecap({
  question,
  question_index,
  uncryptedBallot,
  t,
}) {
  const questionText = question.title;
  const questionPossibleAnswers = question.answers;
  const renderedAnswers = uncryptedBallot[question_index].map(
    function (answer, answer_index) {
      const index =
        question.blankVoteIsAllowed === true ? answer_index - 1 : answer_index;
      return e(
        Grid,
        { item: true, xs: 12 },
        e(
          Grid,
          {
            container: true,
            className:
              "cai-table-vote-option" +
              (answer === 1 ? " cai-table-vote-option-selected" : ""),
          },
          e(
            Grid,
            {
              item: true,
              xs: 8,
              md: 10,
              className: "cai-table-vote-option-text",
            },
            answer_index === 0 && question.blankVoteIsAllowed === true
              ? t("blank_vote")
              : markup(questionPossibleAnswers[index]),
          ),
          e(
            Grid,
            {
              item: true,
              xs: 4,
              md: 2,
              className:
                "cai-table-answer" +
                (answer === 1 ? " cai-table-answer-selected" : ""),
            },
            answer === 1
              ? e(
                  "span",
                  { className: "material-symbols-outlined" },
                  "check_box",
                )
              : e(
                  "span",
                  { className: "material-symbols-outlined" },
                  "disabled_by_default",
                ),
          ),
        ),
      );
    },
  );
  const renderedAnswersBlankAtEnd =
    question.blankVoteIsAllowed === true
      ? [
          ...renderedAnswers.slice(1),
          e(Grid, { item: true, xs: 12 }, " "),
          renderedAnswers[0],
        ]
      : renderedAnswers;

  const renderedVoteToQuestion = e(
    React.Fragment,
    null,
    e(
      "h3",
      {
        className: "whole-vote-recap__question-title",
      },
      markup(questionText),
    ),
    e(
      Grid,
      {
        container: true,
        rowSpacing: 1,
        className: "cai-table",
      },
      ...renderedAnswersBlankAtEnd,
    ),
  );
  return e(
    "div",
    {
      className: "classic-vote-recap",
    },
    renderedVoteToQuestion,
  );
}

const ClassicVoteRecap = withTranslation()(TranslatableClassicVoteRecap);

export { ClassicVoteRecap, TranslatableClassicVoteRecap };
export default ClassicVoteRecap;
