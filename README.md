# Belenios with Cast-As-Intended

This repo is a fork of [Belenios](https://gitlab.inria.fr/belenios/belenios).

This project is a proof of concept and __should not be used in production__.


## Getting started

The following procedure lets you run a demo server on `localhost`.

Note: if you have built the project already, `make clean` will avoid copying everything in the image.

```
docker build -f Dockerfile_base_environment -t beleniosbase .
docker build -f Dockerfile_belenios_demo -t belenios .
docker run --rm --network host -it belenios
```

In order to simplify the setup, emails are written in a file accessible at [localhost:8001/static/mail.txt](http://localhost:8001/static/mail.txt). Note that the content is Quoted-Printable encoded, you might need the following to decode the link to the ballot box: 
```
curl http://localhost:8001/static/mail.txt | python -c 'import sys,quopri;quopri.decode(sys.stdin,sys.stdout.buffer)'
```

## Create an election

1. Visite the [admin interface](http://localhost:8001/static/admin.html) and use demo auth to log in (with empty username)
2. Create a new election
3. Leave the default type of question to have homomorphic tallying
4. Add some voters
5. Generate the decryption key of the server
6. Let the server manage the credentials, generate and download them
7. Select "Dummy auth" as voter authentication
8. Create the election

<img src="screenshot.png" width="700">