#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: belenios 2.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-04 12:48+0000\n"
"PO-Revision-Date: 2023-03-26 11:37+0000\n"
"Last-Translator: \"J. Lavoie\" <j.lavoie@net-c.ca>\n"
"Language-Team: French <https://hosted.weblate.org/projects/belenios/voter/fr/"
">\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.17-dev\n"

#: src/web/server/common/pages_common.ml:333
msgid " (if you forgot it, for example)."
msgstr " (si vous l'oubliez, par exemple)."

#: src/web/server/common/web_common.ml:101
msgid " day(s)"
msgstr " jour(s)"

#: src/web/server/common/pages_voter.ml:176
msgid " for more information."
msgstr " pour plus d'information."

#: src/web/server/common/web_common.ml:103
msgid " hour(s)"
msgstr " heure(s)"

#: src/web/server/common/pages_common.ml:554
msgid " in the following box: "
msgstr " dans la boîte suivante : "

#: src/web/server/common/pages_voter.ml:1103
msgid " is rejected, because "
msgstr " est refusé, parce que "

#: src/web/server/common/web_common.ml:104
msgid " minute(s)"
msgstr " minute(s)"

#: src/web/server/common/web_common.ml:100
msgid " month(s)"
msgstr " mois"

#: src/web/server/common/web_common.ml:105
msgid " second(s)"
msgstr " seconde(s)"

#: src/web/server/common/web_common.ml:99
msgid " year(s)"
msgstr " année(s)"

#: src/web/server/common/pages_voter.ml:133
#. src/web/server/common/pages_voter.ml:101
msgid "%d blank ballot(s)"
msgstr "%d bulletin(s) blanc(s)"

#: src/web/server/common/pages_voter.ml:200
#. src/web/server/common/pages_voter.ml:109
msgid "%d invalid ballot(s)"
msgstr "%d bulletin(s) non valide(s)"

#: src/web/server/common/pages_voter.ml:666
msgid "%d of the following %d trustees (verification keys) [public keys] are needed to decrypt the election result:"
msgstr ""
"%d des %d autorités de confiance suivantes (clés de vérification) [clés "
"publiques] sont nécessaires pour déchiffrer le résultat de l'élection :"

#: src/web/server/common/pages_voter.ml:127
#. src/web/server/common/pages_voter.ml:95
msgid "%d valid (non-blank) ballot(s)"
msgstr "%d bulletin(s) valide(s) (non blancs)"

#: src/web/server/common/pages_voter.ml:128
#. src/web/server/common/pages_voter.ml:96
msgid "%d valid ballot(s)"
msgstr "%d bulletin(s) valide(s)"

#: src/web/server/common/pages_common.ml:329
msgid ", or "
msgstr ", ou "

#: src/web/server/common/pages_voter.ml:1210
msgid "1 is the highest grade, 2 is the second highest grade, etc."
msgstr ""
"1 est la note la plus élevée, 2 est la deuxième note la plus élevée, etc."

#: src/web/server/common/pages_voter.ml:163
msgid "A Condorcet winner is a candidate that is preferred over all the other candidates."
msgstr ""
"Un gagnant Condorcet est un candidat qui est préféré à tous les autres "
"candidats."

#: src/web/server/common/pages_voter.ml:208
msgid "A ballot is invalid if two candidates have been given the same preference order or if a rank is missing."
msgstr ""
"Un bulletin de vote n’est pas valide si deux candidats ont reçu le même "
"ordre de préférence ou si un ordre de préférence est manquant."

#: src/web/server/common/pages_common.ml:442
msgid "A verification code has been sent to %s."
msgstr "Un code de vérification a été envoyé à %s."

#: src/web/server/common/pages_voter.ml:590
msgid "About this voting session"
msgstr "À propos de ce vote"

#: src/web/server/common/pages_voter.ml:1060
msgid "About your ballot"
msgstr "À propos de votre bulletin"

#: src/web/server/common/pages_voter.ml:544
msgid "Accept"
msgstr "Accepter"

#: src/web/server/common/pages_voter.ml:115
msgid "According to Majority Judgment, the ranking is:"
msgstr "Selon le jugement majoritaire, le classement est le suivant :"

#: src/web/server/common/pages_common.ml:99
msgid "Administer elections"
msgstr "Administrer des élections"

#: src/web/server/common/pages_common.ml:103
msgid "Administer this election"
msgstr "Administrer cette élection"

#: src/web/server/common/pages_voter.ml:597
msgid "Administrator"
msgstr "Administrateur"

#: src/web/server/common/pages_common.ml:111
msgid "Advanced booth. "
msgstr "Isoloir avancé. "

#: src/web/server/common/pages_voter.ml:651
msgid "All of the following trustees (verification keys) are needed to decrypt the result:"
msgstr ""
"Toutes les autorités de confiance suivantes (clés de vérification) sont "
"nécessaires pour déchiffrer le résultat :"

#: src/web/server/common/pages_voter.ml:890
#. src/web/server/common/pages_voter.ml:885
msgid "Answer to questions"
msgstr "Réponse aux questions"

#: src/web/server/common/pages_voter.ml:1213
msgid "As a convenience, 0 is always interpreted as the lowest grade."
msgstr ""
"Pour plus de facilité, 0 est toujours interprété comme la note la plus basse."

#: src/web/server/common/pages_voter.ml:59
msgid "Audit data: "
msgstr "Données d'audit : "

#: src/web/server/common/pages_voter.ml:944
#. src/web/server/common/pages_voter.ml:939
#. src/web/server/common/pages_common.ml:472
#. src/web/server/common/pages_common.ml:397
#. src/web/server/common/pages_common.ml:306
msgid "Authenticate"
msgstr "Authentification"

#: src/web/server/common/pages_common.ml:242
msgid "Authenticate with %s"
msgstr "Authentifier avec %s"

#: src/web/server/common/pages_common.ml:411
msgid "Authentication failed"
msgstr "Échec de l'authentification"

#: src/web/server/common/pages_common.ml:417
msgid "Authentication failed, probably because of a bad username or password, or you are not allowed to perform this operation."
msgstr ""
"Échec de l'authentification, probablement à cause d'un mauvais identifiant "
"ou mot de passe, ou alors vous n'êtes pas autorisé à effectuer cette "
"opération."

#: src/web/server/common/pages_common.ml:579
msgid "Authentication impossible"
msgstr "Authentification impossible"

#: src/web/server/common/pages_common.ml:574
msgid "Authentication is impossible."
msgstr "L'authentification est impossible."

#: src/web/server/common/pages_voter.ml:321
msgid "Available methods on this server:"
msgstr "Méthodes disponibles sur ce serveur :"

#: src/web/server/common/pages_common.ml:530
msgid "Bad e-mail address!"
msgstr "Mauvaise adresse courriel !"

#: src/web/server/common/pages_common.ml:529
msgid "Bad security code!"
msgstr "Mauvais code de sécurité !"

#: src/web/server/common/site_voter.ml:252
msgid "Ballot with mask does not match stored ballot."
msgstr ""

#: src/web/server/common/pages_common.ml:513
msgid "Belenios Server"
msgstr "Serveur Belenios"

#: src/web/server/common/pages_common.ml:515
msgid "Belenios authentication"
msgstr "Authentification Belenios"

#: src/web/server/common/pages_common.ml:508
msgid "Best regards,"
msgstr "Cordialement,"

#: src/web/server/common/pages_voter.ml:264
msgid "Blank vote"
msgstr "Vote blanc"

#: src/web/server/common/pages_voter.ml:539
msgid "By using this site, you accept our "
msgstr "En utilisant ce site, vous acceptez notre "

#: src/web/clients/tool/tool_js_fingerprint.ml:92
msgid "Compare"
msgstr "Comparer"

#: src/web/clients/tool/tool_js_fingerprint.ml:74
msgid "Compute fingerprint"
msgstr "Calculer l'empreinte"

#: src/web/clients/tool/tool_js_fingerprint.ml:35
msgid "Computed fingerprint:"
msgstr "Empreinte calculée :"

#: src/web/server/common/pages_voter.ml:1183
msgid "Condorcet-Schulze method"
msgstr "Méthode Condorcet-Schulze"

#: src/web/server/common/pages_voter.ml:997
#. src/web/server/common/pages_voter.ml:992
msgid "Confirm"
msgstr "Confirmation"

#: src/web/server/common/pages_voter.ml:1261
#. src/web/server/common/pages_voter.ml:1199
msgid "Continue"
msgstr "Continuer"

#: src/web/server/common/site_voter.ml:225 src/web/server/common/site_voter.ml:91
msgid "Cookies are blocked"
msgstr "Les cookies sont bloqués"

#: src/web/server/common/pages_voter.ml:679
msgid "Credential's authority"
msgstr "Autorité des codes de vote"

#: src/web/server/common/pages_voter.ml:689
msgid "Credential's fingerprint"
msgstr "Empreinte des code de vote"

#: src/web/server/common/mails_voter.ml:156
msgid "Credential:"
msgstr "Code de vote :"

#: src/web/server/common/pages_common.ml:481
#. src/web/server/common/mails_voter.ml:409
msgid "Dear %s,"
msgstr "Cher/chère %s,"

#: src/web/server/common/pages_common.ml:543
#. src/web/server/common/pages_common.ml:340
#. src/web/server/common/pages_common.ml:273
msgid "E-mail address:"
msgstr "Adresse courriel :"

#: src/web/server/common/pages_voter.ml:56
msgid "Election fingerprint: "
msgstr "Empreinte de l'élection : "

#: src/web/common/pages_common.ml:73 src/web/common/pages_common.ml:64
msgid "Election server"
msgstr "Serveur d'élections"

#: src/web/server/common/pages_voter.ml:1093
msgid "Email sent"
msgstr "Email envoyé"

#: src/web/server/common/site_voter.ml:468
#. src/web/server/common/site_voter.ml:451
#. src/web/server/common/site_voter.ml:409
#. src/web/server/common/site_voter.ml:402
#. src/web/server/common/site_voter.ml:397
#. src/web/server/common/site_voter.ml:283
#. src/web/server/common/site_voter.ml:269
#. src/web/server/common/site_voter.ml:254
#. src/web/server/common/site_voter.ml:234
#. src/web/server/common/site_voter.ml:129
#. src/web/server/common/site_voter.ml:115
#. src/web/server/common/site_voter.ml:98
msgid "Error"
msgstr "Erreur"

#: src/web/clients/tool/tool_js_fingerprint.ml:83
msgid "Expected fingerprint:"
msgstr "Empreinte attendue :"

#: src/web/server/common/pages_voter.ml:1108
msgid "FAIL!"
msgstr "ÉCHEC !"

#: src/web/server/common/pages_voter.ml:721
msgid "Fingerprint of the encrypted tally"
msgstr ""

#: src/web/server/common/pages_voter.ml:1141
#. src/web/server/common/pages_voter.ml:1121
msgid "Follow the link in your "
msgstr "Suivre le lien vers l'urne dans "

#: src/web/server/common/site_common.ml:121
msgid "Forbidden"
msgstr "Interdit"

#: src/web/common/pages_common.ml:111
msgid "Get the source code"
msgstr "Obtenir le code source"

#: src/web/server/common/pages_voter.ml:1041
msgid "Go back to election"
msgstr "Retourner à la page d'accueil de l'élection"

#: src/web/server/common/mails_voter.ml:462
msgid "If this control pattern does not match the one you saved, please contact the administrator indicated below."
msgstr "Si votre bulletin est introuvable ou si votre motif de contrôle ne correspond pas, veuillez contacter l'administrateur mentionné ci-dessous."

#: src/web/server/common/pages_voter.ml:1022
msgid "If you want to vote, you must "
msgstr "Si vous voulez voter, vous devez "

#: src/web/server/common/pages_voter.ml:1130
msgid "If your ballot is missing or the control pattern does not match, contact the administrator: "
msgstr "Si votre bulletin est introuvable ou si votre motif de contrôle ne correspond pas, contacter l'administrateur : "

#: src/web/server/common/pages_voter.ml:1147
msgid "If your ballot is missing, contact the administrator: "
msgstr "Si votre bulletin est introuvable, contacter l'administrateur : "

#: src/web/server/common/site_voter.ml:235 src/web/server/common/site_voter.ml:99
msgid "Ill-formed ballot"
msgstr "Bulletin de vote mal formé"

#: src/web/server/common/pages_voter.ml:234
msgid "In our implementation, when several candidates have the same number of votes when they are ready to be elected or eliminated, we follow the order in which candidates were listed in the election."
msgstr ""
"Dans notre implémentation, lorsque plusieurs candidats ont le même nombre de "
"voix alors qu'ils sont prêts à être élus ou éliminés, nous suivons l'ordre "
"dans lequel les candidats ont été listés lors de l'élection."

#: src/web/server/common/pages_voter.ml:1206
msgid "In the context of Majority Judgment, a vote gives a grade to each candidate."
msgstr ""
"Dans le cadre du jugement majoritaire, un électeur donne une note à chaque "
"candidat."

#: src/web/server/common/pages_voter.ml:1268
msgid "In the context of STV, voters rank candidates by order of preference."
msgstr ""
"Dans le cadre de STV, les électeurs classent les candidats par ordre de "
"préférence."

#: src/web/server/common/pages_voter.ml:863
#. src/web/server/common/pages_voter.ml:858
msgid "Input credential"
msgstr "Saisie du code de vote"

#: src/web/server/common/site_voter.ml:410
msgid "Invalid index for question."
msgstr "Numéro de question invalide."

#: src/web/server/common/pages_voter.ml:349
msgid "It contains all submitted ballots in clear, in random order."
msgstr ""
"Il contient tous les bulletins soumis en clair, dans un ordre aléatoire."

#: src/web/server/common/pages_voter.ml:319
msgid "It is up to you to apply your favorite counting method."
msgstr "C'est à vous d'appliquer votre méthode de comptage préférée."

#: src/web/server/common/pages_voter.ml:375
msgid "It will open in "
msgstr "Elle ouvrira dans "

#: src/web/server/common/pages_voter.ml:345
msgid "JSON result"
msgstr "résultat JSON"

#: src/web/server/common/pages_common.ml:166
msgid "Language:"
msgstr "Langue :"

#: src/web/server/common/pages_common.ml:471
#. src/web/server/common/pages_common.ml:396
#. src/web/server/common/pages_common.ml:305
#. src/web/server/common/pages_common.ml:265
msgid "Log in"
msgstr "Connexion"

#: src/web/server/common/pages_common.ml:241
msgid "Log in with %s"
msgstr "Se connecter avec %s"

#: src/web/server/common/pages_voter.ml:245
msgid "Look at the raw events for more details."
msgstr "Regardez les événements bruts pour plus de détails."

#: src/web/server/common/pages_voter.ml:326
msgid "Majority Judgment"
msgstr "Jugement majoritaire"

#: src/web/server/common/pages_voter.ml:1245
#. src/web/server/common/pages_voter.ml:1190
msgid "Majority Judgment method"
msgstr "Méthode du jugement majoritaire"

#: src/web/server/common/pages_voter.ml:229
msgid "Many variants of STV exist, depending for example on how to break ties."
msgstr ""
"De nombreuses variantes de STV existent, qui dépendent par exemple de la "
"manière de gérer les cas d'égalité."

#: src/web/server/common/pages_voter.ml:1282
msgid "Many variants of STV exist, we documented our choices in "
msgstr ""
"De nombreuses variantes de STV existent, nous avons documenté nos choix dans "

#: src/web/server/common/pages_common.ml:576
msgid "Maybe cookies are blocked."
msgstr "Les cookies sont peut-être bloqués."

#: src/web/server/common/pages_voter.ml:1278
#. src/web/server/common/pages_voter.ml:1220
msgid "More information can be found "
msgstr "Pour plus d'informations, voir "

#: src/web/server/common/pages_voter.ml:1115
msgid "Next steps"
msgstr "Prochaines étapes"

#: src/web/server/common/site_common.ml:53
msgid "Not found"
msgstr "Non trouvé"

#: src/web/server/common/mails_voter.ml:58
msgid "Note that you also need a credential, sent in a separate email, to start voting."
msgstr ""
"Notez que vous avez également besoin d'un code de vote, envoyé dans un "
"courriel séparé, pour commencer à voter."

#: src/web/server/common/pages_voter.ml:503
msgid "Number of accepted ballots: "
msgstr "Nombre de bulletins acceptés : "

#: src/web/server/common/pages_voter.ml:1196
msgid "Number of grades:"
msgstr "Nombre de notes :"

#: src/web/server/common/pages_voter.ml:1258
msgid "Number of seats:"
msgstr "Nombre de sièges :"

#: src/web/server/common/pages_voter.ml:606
msgid "Number of voters"
msgstr "Nombre de votants"

#: src/web/server/common/mails_voter.ml:167
#. src/web/server/common/mails_voter.ml:74
msgid "Number of votes:"
msgstr "Nombre de votes :"

#: src/web/server/common/mails_voter.ml:178
#. src/web/server/common/mails_voter.ml:85
msgid "Only the last vote counts."
msgstr "Seul le dernier vote est pris en compte."

#: src/web/server/common/mails_voter.ml:172
#. src/web/server/common/mails_voter.ml:79
msgid "Page of the election:"
msgstr "Page de l'élection :"

#: src/web/server/common/pages_common.ml:376
#. src/web/server/common/mails_voter.ml:67
msgid "Password:"
msgstr "Mot de passe :"

#: src/web/server/common/pages_common.ml:552
msgid "Please enter "
msgstr "Veuillez entrer "

#: src/web/server/common/pages_common.ml:454
msgid "Please enter the verification code received by e-mail:"
msgstr "Veuillez entrer le code de vérification reçu par courriel :"

#: src/web/server/common/mails_voter.ml:50
msgid "Please find below your login and password for the election"
msgstr ""
"Veuillez trouver ci-dessous votre identifiant et votre mot de passe pour "
"l'élection"

#: src/web/server/common/pages_common.ml:259
msgid "Please log in:"
msgstr "Veuillez vous connecter :"

#: src/web/clients/tool/tool_js_fingerprint.ml:61
msgid "Please paste the data for which you want to compute the fingerprint in the text area below:"
msgstr ""
"Veuillez coller les données pour lesquelles vous souhaitez calculer "
"l'empreinte dans la zone de texte ci-dessous :"

#: src/web/server/common/pages_voter.ml:1234
msgid "Please provide the number of grades to see the result of the election according to the Majority Judgment method."
msgstr ""
"Veuillez indiquer le nombre de notes pour voir le résultat de l’élection "
"selon la méthode du jugement majoritaire."

#: src/web/server/common/pages_voter.ml:1293
msgid "Please provide the number of seats to see the result of the election according to the Single Transferable Vote method."
msgstr ""
"Veuillez indiquer le nombre de sièges pour voir le résultat de l'élection "
"selon la méthode du Single Transferable Vote."

#: src/web/common/pages_common.ml:105
msgid "Powered by "
msgstr "Propulsé par "

#: src/web/common/pages_common.ml:115
msgid "Privacy policy"
msgstr "Politique de confidentialité"

#: src/web/server/common/pages_common.ml:223
msgid "Proceed"
msgstr "Continuer"

#: src/web/server/common/pages_voter.ml:216
msgid "Raw events"
msgstr "Evénements bruts"

#: src/web/server/common/mails_voter.ml:468
msgid "Results will be published on the election page"
msgstr "Les résultats seront publiés sur la page de l'élection"

#: src/web/server/common/pages_voter.ml:917
#. src/web/server/common/pages_voter.ml:912
msgid "Review and encrypt"
msgstr "Récapitulatif et chiffrement"

#: src/web/server/common/pages_voter.ml:1078
msgid "Revote"
msgstr ""

#: src/web/server/common/pages_voter.ml:974
#. src/web/server/common/pages_voter.ml:969
msgid "Security check"
msgstr "Contrôle de sécurité"

#: src/web/server/common/pages_voter.ml:435
msgid "See accepted ballots"
msgstr "Voir les bulletins acceptés"

#: src/web/server/common/pages_common.ml:173
msgid "Set"
msgstr "Appliquer"

#: src/web/server/common/pages_voter.ml:168
msgid "Several techniques exist to decide which candidate to elect when there is no Condorcet winner."
msgstr ""
"Plusieurs techniques existent pour décider quel candidat élire lorsqu'il n'y "
"a pas de vainqueur de Condorcet."

#: src/web/server/common/pages_voter.ml:330
msgid "Single Transferable Vote"
msgstr "Vote unique transférable (Single Tranferable Vote)"

#: src/web/server/common/pages_voter.ml:1304
#. src/web/server/common/pages_voter.ml:1252
msgid "Single Transferable Vote method"
msgstr "Méthode du vote unique transférable (Single Transferable Vote)"

#: src/web/server/common/pages_voter.ml:456
msgid "Start"
msgstr "Commencer"

#: src/web/server/common/pages_voter.ml:1073
msgid "Status"
msgstr "Statut"

#: src/web/server/common/pages_voter.ml:865
msgid "Step 1"
msgstr "Étape 1"

#: src/web/server/common/pages_voter.ml:892
msgid "Step 2"
msgstr "Étape 2"

#: src/web/server/common/pages_voter.ml:919
msgid "Step 3"
msgstr "Étape 3"

#: src/web/server/common/pages_voter.ml:946
msgid "Step 4"
msgstr "Étape 4"

#: src/web/server/common/pages_voter.ml:976
msgid "Step 5"
msgstr "Étape 5"

#: src/web/server/common/pages_voter.ml:999
msgid "Step 6"
msgstr ""

#: src/web/server/common/pages_common.ml:557
#. src/web/server/common/pages_common.ml:463
msgid "Submit"
msgstr "Soumettre"

#: src/web/server/common/pages_voter.ml:241
msgid "Such candidates are marked as \"TieWin\" when they are elected and as \"TieLose\" if they have lost."
msgstr ""
"Ces candidats sont marqués comme « TieWin » lorsqu'ils sont élus et comme « "
"TieLose » s'ils ont perdu."

#: src/web/server/common/pages_voter.ml:1100
msgid "Thank you for voting!"
msgstr "Votre bulletin a été envoyé"

#: src/web/server/common/pages_voter.ml:181
msgid "The Schulze winners are:"
msgstr "Les vainqueurs au sens de Schulze sont :"

#: src/web/server/common/pages_voter.ml:250
msgid "The Single Transferable Vote winners are:"
msgstr "Les gagnants selon la méthode Single Transferable Vote sont :"

#: src/web/server/common/pages_voter.ml:405
#. src/web/server/common/pages_voter.ml:401
msgid "The election is closed and being tallied."
msgstr "L'élection est fermée et en cours de dépouillement."

#: src/web/server/common/pages_voter.ml:392
msgid "The election will close in "
msgstr "L'élection fermera dans "

#: src/web/clients/tool/tool_js_fingerprint.ml:47
msgid "The fingerprints differ!"
msgstr "Les empreintes diffèrent !"

#: src/web/clients/tool/tool_js_fingerprint.ml:46
msgid "The fingerprints match!"
msgstr "Les empreintes correspondent !"

#: src/web/server/common/pages_voter.ml:1229
msgid "The number of different grades (Excellent, Very Good, etc.) typically varies from 5 to 7."
msgstr ""
"Le nombre de notes différentes (Excellent, Très bon, etc.) varie "
"généralement de 5 à 7."

#: src/web/server/common/site_voter.ml:452
msgid "The number of grades is invalid."
msgstr "Le nombre de notes n'est pas valide."

#: src/web/server/common/site_voter.ml:469
msgid "The number of seats is invalid."
msgstr "Le nombre de sièges n’est pas valide."

#: src/web/server/common/pages_voter.ml:343
msgid "The raw results can be viewed in the "
msgstr "Les résultats bruts peuvent être consultés dans le "

#: src/web/server/common/pages_voter.ml:526
msgid "The result of this election is currently not publicly available. It will be in %s."
msgstr "Le résultat de cette élection n'est actuellement pas public. Il le sera dans %s."

#: src/web/server/common/site_voter.ml:398
msgid "The result of this election is not available."
msgstr "Le résultat de cette élection n'est pas disponible."

#: src/web/server/common/pages_common.ml:501
msgid "The tracking number of your ballot is:"
msgstr "Votre numéro de suivi de votre bulletin est:"

#: src/web/server/common/pages_voter.ml:1216
msgid "The winner is the candidate with the highest median (or the 2nd highest median if there is a tie, etc.)."
msgstr ""
"Le gagnant est le candidat qui a la médiane la plus élevée (ou la 2e médiane "
"la plus élevée en cas d'égalité, etc.)."

#: src/web/server/common/pages_voter.ml:226
msgid "There has been at least one tie."
msgstr "Il y a eu au moins une égalité."

#: src/web/server/common/site_common.ml:54
msgid "This election does not exist. This may happen for elections that have not yet been open or have been deleted."
msgstr "Cette élection n'existe pas. Cela peut arriver quand l'élection n'a pas encore été ouverte ou a été supprimée."

#: src/web/server/common/pages_voter.ml:408
msgid "This election has been tallied."
msgstr "Cette élection a été dépouillée."

#: src/web/server/common/pages_voter.ml:409
msgid "This election is archived."
msgstr "Cette élection est archivée."

#: src/web/server/common/pages_voter.ml:383
msgid "This election is currently closed."
msgstr "Cette élection est actuellement fermée."

#: src/web/server/common/pages_voter.ml:554
msgid "This election uses weights!"
msgstr "Cette élection utilise des votes pondérés !"

#: src/web/server/common/site_voter.ml:403
msgid "This question is homomorphic, this method cannot be applied to its result."
msgstr ""
"La question demandait de sélectionner des candidats et ne permet pas "
"d'appliquer cette méthode."

#: src/web/server/common/mails_voter.ml:433
msgid "This vote replaces any previous vote."
msgstr "Ce vote remplace le vote précédent."

#: src/web/server/common/pages_voter.ml:152
#. src/web/server/common/pages_voter.ml:87
msgid "Tie:"
msgstr "Égalité :"

#: src/web/server/common/mails_voter.ml:151
msgid "To cast a vote, you will also need a password, sent in a separate email."
msgstr ""
"Pour soumettre un bulletin, vous aurez également besoin d'un mot de passe, "
"envoyé dans un courriel séparé."

#: src/web/server/common/mails_voter.ml:40
msgid "To get more information, please contact:"
msgstr "Pour obtenir plus d'informations, veuillez contacter :"

#: src/web/server/common/pages_voter.ml:488
msgid "Total weight of accepted ballots:"
msgstr "Nombre total de voix pour les bulletins acceptés :"

#: src/web/server/common/pages_voter.ml:1068
msgid "Tracking number"
msgstr "Numéro de suivi"

#: src/web/server/common/pages_voter.ml:708
msgid "Trustees shuffled the ballots in the following order:"
msgstr ""
"Les autorités de confiance suivantes ont successivement procédé au mélange "
"des bulletins :"

#: src/web/server/common/site_voter.ml:282
#. src/web/server/common/site_voter.ml:128
msgid "Unknown election"
msgstr "Élection inconnue"

#: src/web/server/common/pages_voter.ml:457
msgid "Unsupported booth version"
msgstr "Version de l'isoloir non prise en charge"

#: src/web/server/common/pages_common.ml:488
msgid "Use the following code:"
msgstr "Utilisez le code suivant :"

#: src/web/server/common/pages_common.ml:339
#. src/web/server/common/pages_common.ml:272
#. src/web/server/common/mails_voter.ml:161
#. src/web/server/common/mails_voter.ml:63
msgid "Username:"
msgstr "Nom d'utilisateur :"

#: src/web/server/common/pages_voter.ml:1126
msgid "Verify your control pattern"
msgstr "Verifier votre modèle de sécurité"

#: src/web/server/common/pages_voter.ml:1065
msgid "Voter"
msgstr "Votant"

#: src/web/server/common/pages_voter.ml:615
msgid "Voter list fingerprint"
msgstr "Empreinte de la liste électorale"

#: src/web/server/common/pages_voter.ml:631
msgid "Voters weight"
msgstr ""

#: src/web/server/common/pages_voter.ml:1033
msgid "Warning:"
msgstr "Attention :"

#: src/web/server/common/pages_common.ml:496
msgid "Warning: this code is valid for 15 minutes, and previous codes sent to this address are no longer valid."
msgstr ""
"Attention : ce code est valable pendant 15 minutes, et les codes précédents "
"envoyés à cette adresse ne sont plus valables."

#: src/web/server/common/pages_voter.ml:172
msgid "We use here the Schulze method and we refer voters to "
msgstr ""
"Nous utilisons ici la méthode de Schulze et nous renvoyons les électeurs à "

#: src/web/server/common/pages_voter.ml:1085
msgid "Weight"
msgstr "Nombre de voix"

#: src/web/server/common/pages_voter.ml:1273
msgid "When a candidate obtains enough votes to be elected, the votes are transferred to the next candidate in the voter ballot, with a coefficient proportional to the \"surplus\" of votes."
msgstr ""
"Lorsqu’un candidat obtient suffisamment de voix pour être élu, les votes "
"sont transférés au candidat suivant dans le bulletin de vote, avec un "
"coefficient proportionnel à l'« excédent » des voix."

#: src/web/server/common/pages_common.ml:190
msgid "Wish to help with translations?"
msgstr "Vous souhaitez aider à traduire ?"

#: src/web/server/common/mails_voter.ml:177
#. src/web/server/common/mails_voter.ml:84
msgid "You are allowed to vote several times."
msgstr "Vous avez le droit de voter plusieurs fois."

#: src/web/server/common/mails_voter.ml:137
msgid "You are listed as a voter for the election"
msgstr "Vous êtes enregistré(e) en tant qu'électeur(trice) pour l'élection"

#: src/web/server/common/site_common.ml:120
msgid "You are not allowed to access this page!"
msgstr "Vous n'avez pas le droit d'accéder à cette page !"

#: src/web/server/common/pages_common.ml:423
msgid "You can "
msgstr "Vous pouvez "

#: src/web/server/common/pages_common.ml:327
msgid "You can also "
msgstr "Vous pouvez aussi "

#: src/web/server/common/pages_voter.ml:509
msgid "You can also download the "
msgstr "Vous pouvez également télécharger le "

#: src/web/server/common/mails_voter.ml:437
msgid "You can check its presence in the ballot box, accessible at"
msgstr "Vous pouvez vérifier sa présence dans l'urne, accessible au"

#: src/web/server/common/pages_common.ml:567
msgid "You cannot log in now. Please try later."
msgstr ""
"Vous ne pouvez pas vous connecter maintenant. Veuillez essayer plus tard."

#: src/web/server/common/mails_voter.ml:146
msgid "You will be asked to enter your credential before entering the voting booth."
msgstr "Le système vous demandera votre code de vote dès l'entrée dans l'isoloir virtuel."

#: src/web/server/common/mails_voter.ml:144
msgid "You will find below your credential."
msgstr "Veuillez trouver ci-dessous votre code de vote."

#: src/web/server/common/site_voter.ml:266
#. src/web/server/common/site_voter.ml:112
msgid "Your ballot is rejected because %s."
msgstr "Votre bulletin est rejeté car %s."

#: src/web/server/common/site_voter.ml:226 src/web/server/common/site_voter.ml:92
msgid "Your browser seems to block cookies. Please enable them."
msgstr "Votre navigateur semble bloquer les cookies. Veuillez les activer."

#: src/web/server/common/mails_voter.ml:445
msgid "Your control pattern is"
msgstr "Votre motif de contrôle est"

#: src/web/server/common/mails_voter.ml:198
msgid "Your credential for election %s"
msgstr "Votre code de vote pour l'élection %s"

#: src/web/server/common/pages_common.ml:485
msgid "Your e-mail address has been used to authenticate with our Belenios server."
msgstr ""
"Votre adresse courriel a été utilisée pour vous authentifier auprès de notre "
"serveur Belenios."

#: src/web/server/common/mails_voter.ml:104
msgid "Your password for election %s"
msgstr "Votre mot de passe pour l'élection %s"

#: src/web/server/common/mails_voter.ml:425
msgid "Your tracking number is"
msgstr "Votre numéro de suivi est"

#: src/web/server/common/mails_voter.ml:412
msgid "Your vote for election"
msgstr "Votre vote pour l'élection"

#: src/web/server/common/site_voter.ml:177
msgid "Your vote for election %s"
msgstr "Votre vote pour l'élection %s"

#: src/web/server/common/pages_voter.ml:1035
msgid "Your vote was not recorded!"
msgstr "Votre vote n'a pas été enregistré !"

#: src/web/server/common/mails_voter.ml:423
msgid "Your weight is %s."
msgstr "Votre poids est de %s."

#: src/web/server/common/pages_voter.ml:1073
msgid "accepted"
msgstr "accepté"

#: src/web/server/common/web_common.ml:86
msgid "ballot after audit mask selection does not match"
msgstr ""

#: src/web/server/common/pages_common.ml:331
msgid "change your password"
msgstr "changez votre mot de passe"

#: src/web/server/common/pages_voter.ml:1142
#. src/web/server/common/pages_voter.ml:1124
msgid "confirmation email"
msgstr "l'email de confirmation"

#: src/web/server/common/pages_common.ml:328
msgid "create an account"
msgstr "créer un compte"

#: src/web/server/common/mails_voter.ml:419
msgid "has been recorded."
msgstr "a été enregistré avec succès."

#: src/web/server/common/pages_voter.ml:1280
#. src/web/server/common/pages_voter.ml:1221
msgid "here"
msgstr "ici"

#: src/web/server/common/pages_voter.ml:1094
msgid "no"
msgstr "non"

#: src/web/server/common/pages_voter.ml:1285
msgid "our code of STV"
msgstr "notre code de STV"

#: src/web/server/common/pages_voter.ml:60
msgid "parameters"
msgstr "paramètres"

#: src/web/server/common/pages_voter.ml:540
msgid "personal data policy"
msgstr "politique concernant les données personnelles"

#: src/web/server/common/pages_voter.ml:64
msgid "public data"
msgstr "données publiques"

#: src/web/server/common/pages_voter.ml:511
msgid "raw result"
msgstr "résultat brut"

#: src/web/server/common/web_common.ml:75
msgid "some proofs failed verification"
msgstr "certaines preuves sont invalides"

#: src/web/server/common/pages_voter.ml:1024
msgid "start from the beginning"
msgstr "recommencer au début"

#: src/web/server/common/pages_voter.ml:175
msgid "the Wikipedia page"
msgstr "la page Wikipédia"

#: src/web/server/common/web_common.ml:68
msgid "the election is closed"
msgstr "l'élection est fermée"

#: src/web/server/common/web_common.ml:82
msgid "this ballot has already been accepted"
msgstr "ce bulletin a déjà été accepté"

#: src/web/server/common/web_common.ml:83
msgid "this ballot has expired"
msgstr "ce bulletin a expiré"

#: src/web/server/common/pages_common.ml:424
msgid "try to log in again"
msgstr "essayer de vous connecter à nouveau"

#: src/web/server/common/pages_voter.ml:1094
#. src/web/server/common/pages_voter.ml:1078
msgid "yes"
msgstr "oui"

#: src/web/server/common/web_common.ml:77
msgid "you are not allowed to revote"
msgstr "vous n'êtes pas autorisé(e) à revoter"

#: src/web/server/common/web_common.ml:69
msgid "you are not allowed to vote"
msgstr "vous n'êtes pas autorisé(e) a voter"

#: src/web/server/common/web_common.ml:80
msgid "you are not allowed to vote with this credential"
msgstr "vous n'êtes pas autorisé(e) à voter avec ce code de vote"

#: src/web/server/common/web_common.ml:72
msgid "your ballot has a syntax error (%s)"
msgstr "votre bulletin a une erreur de syntaxe (%s)"

#: src/web/server/common/web_common.ml:74
msgid "your ballot is not in canonical form"
msgstr "votre bulletin n'est pas sous forme canonique"

#: src/web/server/common/web_common.ml:81
msgid "your credential has a bad weight"
msgstr "Votre code de vote ne correspond pas au bon nombre de voix"

#: src/web/server/common/web_common.ml:78
msgid "your credential has already been used"
msgstr "votre code de vote a déjà été utilisé"

#: src/web/server/common/web_common.ml:76
msgid "your credential is invalid"
msgstr "votre code de vote est invalide"

#: src/web/server/common/web_common.ml:84
msgid "your username is wrong"
msgstr "votre nom d'utilisateur est incorrect"

